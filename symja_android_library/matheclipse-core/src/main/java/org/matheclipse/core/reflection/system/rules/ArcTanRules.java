package org.matheclipse.core.reflection.system.rules;

import static org.matheclipse.core.expression.F.*;
import org.matheclipse.core.interfaces.IAST;

/**
 * <p>Generated by <code>org.matheclipse.core.preprocessor.RulePreprocessor</code>.</p>
 * <p>See GIT repository at: <a href="https://bitbucket.org/axelclk/symja_android_library">bitbucket.org/axelclk/symja_android_library under the tools directory</a>.</p>
 */
public interface ArcTanRules {
  /**
   * <ul>
   * <li>index 0 - number of equal rules in <code>RULES</code></li>
	 * </ul>
	 */
  final public static int[] SIZES = { 18, 0 };

  final public static IAST RULES = List(
    IInit(ArcTan, SIZES),
    // ArcTan(0)=0
    ISet(ArcTan(C0),
      C0),
    // ArcTan(0,0)=0
    ISet(ArcTan(C0,C0),
      C0),
    // ArcTan(2-Sqrt(3))=Pi/12
    ISet(ArcTan(Plus(C2,Negate(CSqrt3))),
      Times(QQ(1L,12L),Pi)),
    // ArcTan(Sqrt(2)+(-1)*1)=Pi/8
    ISet(ArcTan(Plus(CN1,CSqrt2)),
      Times(QQ(1L,8L),Pi)),
    // ArcTan(1/Sqrt(3))=Pi/6
    ISet(ArcTan(C1DSqrt3),
      Times(QQ(1L,6L),Pi)),
    // ArcTan(Sqrt(5-2*Sqrt(5)))=Pi/5
    ISet(ArcTan(Sqrt(Plus(C5,Times(CN2,CSqrt5)))),
      Times(QQ(1L,5L),Pi)),
    // ArcTan(1)=Pi/4
    ISet(ArcTan(C1),
      Times(C1D4,Pi)),
    // ArcTan(1,1)=Pi/4
    ISet(ArcTan(C1,C1),
      Times(C1D4,Pi)),
    // ArcTan(-1,-1)=-3/4*Pi
    ISet(ArcTan(CN1,CN1),
      Times(QQ(-3L,4L),Pi)),
    // ArcTan(Sqrt(3))=Pi/3
    ISet(ArcTan(CSqrt3),
      Times(C1D3,Pi)),
    // ArcTan(1+Sqrt(2))=3/8*Pi
    ISet(ArcTan(Plus(C1,CSqrt2)),
      Times(QQ(3L,8L),Pi)),
    // ArcTan(2+Sqrt(3))=5/12*Pi
    ISet(ArcTan(Plus(C2,CSqrt3)),
      Times(QQ(5L,12L),Pi)),
    // ArcTan(I)=I*Infinity
    ISet(ArcTan(CI),
      DirectedInfinity(CI)),
    // ArcTan(Infinity,y_)=0
    ISet(ArcTan(oo,y_),
      C0),
    // ArcTan(Infinity)=Pi/2
    ISet(ArcTan(oo),
      Times(C1D2,Pi)),
    // ArcTan(-Infinity)=-Pi/2
    ISet(ArcTan(Noo),
      Times(CN1D2,Pi)),
    // ArcTan(I*Infinity)=Pi/2
    ISet(ArcTan(DirectedInfinity(CI)),
      Times(C1D2,Pi)),
    // ArcTan((-1)*I*Infinity)=-Pi/2
    ISet(ArcTan(DirectedInfinity(CNI)),
      Times(CN1D2,Pi))
  );
}
